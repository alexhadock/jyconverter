# jyconverter

Simple in usage script for convert:

- YAML to JSON;
- JSON to YAML.

You don`t need share your file with any of online converters, nobody knows what they do with your data. Just convert this on your machine :) Note: if something wrong with your file - script tell you, what exactly.

## Installation

Install pyyaml:

```
pip3 install pyyaml
```

Download script:

```
git clone https://gitlab.com/alexhadock/jyconverter.git
```

Run:

```
py jyconverter.py -i file1 file2
```