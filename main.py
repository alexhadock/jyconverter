#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import json

import yaml



def detect_file_type(file_name):

    with open(file_name, "r", encoding='utf-8') as in_fh:
        # Read the file into memory as a string so that we can try
        # parsing it twice without seeking back to the beginning and
        # re-reading.
        config = in_fh.read()

    try:
        config_dict = json.loads(config)
        return 'json'
    except:
        try:
            config_dict = yaml.safe_load(config)
            return 'yaml'
        except:
            return None


def convert_file(file_name, file_type):

    if file_type == 'yaml':
        with open(file_name, 'r', encoding='utf-8') as input_file, open(file_name + ".json", "w", encoding='utf-8') as output_file:
            json.dump(
                yaml.safe_load(input_file.read()),
                output_file,
                sort_keys=True,
                indent=4,
                ensure_ascii=False
            )
        print("Created file " + file_name + ".json")
    elif file_type == 'json':
        with open(file_name, 'r', encoding='utf-8') as input_file, open(file_name + ".yml", "w", encoding='utf-8') as output_file:
            yaml.dump(
                json.loads(input_file.read()),
                output_file,
                allow_unicode=True
            )
        print("Created file " + file_name + ".yml")
    else:
        print("Got unexpected file!")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i", help="-i myawesomefile.yourext\nEnter the name of the source file.")
    args = parser.parse_args()
    if args.i:
        convert_file(args.i, detect_file_type(args.i))
    else:
        print("Please, enter the name of the source file.")
